import os
from base64 import b64encode, b64decode
import click
from requests import Session
from bs4 import BeautifulSoup
import re
import json
import io
from csv import DictReader


@click.command()
def main():
    s = Session()
    resp_inital = s.post(
        url="https://mystreamspot.com/login",
        data=f"username={os.environ['USER']}&password={os.environ['PASSWORD']}",
        headers={
            "Content-Type": "application/x-www-form-urlencoded"
        },
        timeout=30,
    )

    resp = s.get(
        url="https://mystreamspot.com/analytics/"
    )
    soup = BeautifulSoup(resp.text, "html.parser")


    ids = [e['href'].split('id=')[1] for e in soup.find_all(href=re.compile("event-statistics"))]
    names = [str(e) for e in soup.find_all(string=re.compile("Unscheduled Event"))]
    
    for i, n in zip(ids, names):
        resp = s.get(
            url="https://mystreamspot.com/analytics/export-event-uniques",
            params=dict(id=i),
        )
        reader = DictReader(io.StringIO(resp.text))
        cnt = 0
        for row in reader:
            if row["viewerType"] == "live":
                cnt += 1
        print(f"{n}: {cnt}")

    print('hi')



if __name__ == "__main__":
    main()